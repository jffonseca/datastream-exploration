
int elementcount;
int numX;

StructuredBuffer<float2> uvBuffer;
StructuredBuffer<float3> posBuffer;
Texture2D tex;
SamplerState s0 <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

struct BufferIn
{
	float3 pos;
    float3 pos2;
	float3 uv;
};

RWStructuredBuffer<BufferIn> Output : BACKBUFFER;





float smooth=0.5;
float maxLength=2;
[numthreads(64, 1, 1)]
void PInPlace( uint3 DTid : SV_DispatchThreadID )
{   
	
if(DTid.x >= asuint(elementcount)) return;

	BufferIn res;
	BufferIn old = Output[DTid.x];
	res.pos=posBuffer[DTid.x];
	float3 newPos = (float3(1,-1,1)*(float3(tex.SampleLevel(s0,uvBuffer[DTid.x].xy,0).xy,0)));
	float mag = length(newPos);
	if (mag>maxLength) newPos=normalize(newPos)*maxLength;
	res.pos2=old.pos2 + (newPos - old.pos2)*0.1;
    res.uv = float3(uvBuffer[DTid.x].xy,0);
	float3 prevPos= Output[DTid.x].pos2.xyz;
	res.pos2.xyz = prevPos+(res.pos2.xyz-prevPos)*smooth;
	Output[DTid.x] = res;
}


technique11 inPlace
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, PInPlace() ) );
	}
}