//@author: sebl
//@help: might become a really quick queue... if finished/working
//@tags: queue
//@credits: vux

Texture3D texture3d <string uiname="Texture3d In";>;
SamplerState volumeSampler  : IMMUTABLE
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

Texture2D texture2d <string uiname="Control Texture";>;

SamplerState maskSampler : IMMUTABLE
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

float4x4 tW: WORLD;
//float4x4 tV: VIEW;
//float4x4 tP: PROJECTION;
float4x4 tVP : VIEWPROJECTION;
float4x4 tWVP: WORLDVIEWPROJECTION;
//float4x4 tVi: VIEWINVERSE;
//float4x4 tWi: WORLDINVERSE;

int sliceIndex <string uiname="slice Index";>;  // the slice
int sliceCount <string uiname="slice Count";>;  // the slice


float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;



//////////////////////// structs  ///////////////////////
struct VS_IN
{
	float4 PosO : POSITION;
	float4 TexCd : TEXCOORD0;	
};

struct vs2ps
{
	float4 PosWVP: SV_POSITION;
	float4 TexCd: TEXCOORD0;
};


//////////////////////// VS  ///////////////////////
vs2ps VS(VS_IN input)
{
	vs2ps Out = (vs2ps)0;
	Out.PosWVP  = mul(input.PosO,mul(tW,tVP));
	Out.TexCd = mul(input.TexCd, tTex);
	return Out;
}

//////////////////////// PS  ///////////////////////
float4 PS(vs2ps In): SV_Target
{
	//get the color of Texture Trigger
	float4 mask = texture2d.Sample(maskSampler, In.TexCd.xy);
	//use those values (they go from 0 to 1) as y value
	//for the texture lookup of the secound texture
	
	float depth = ((mask.r * sliceCount + sliceIndex) %sliceCount) / sliceCount;
	
//	float4 col = texture3d.Sample(volumeSampler, float3(In.TexCd.xy, mask.r));
	float4 col = texture3d.Sample(volumeSampler, float3(In.TexCd.xy, depth));
	return col;
}


//////////////////////// techniques  ///////////////////////
technique10 Distort
{
	pass P0
	{
		SetHullShader( 0 );
		SetDomainShader( 0 );
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}




